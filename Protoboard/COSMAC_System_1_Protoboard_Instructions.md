# COSMAC MICROBOARD BREADBOARD INSTRUCTIONS - CDP18S702

__Version 1.0__


Thank you for purchasing the COSMAC System 1 Microboard Breadboard. This PCB allows for conveniently prototyping or building one-off RCA Microboards. The board contains no circuitry to buffer or gate the CPU signals and remains the responsibility of the designer. If you desire such capability then consider using the Buffered Protoboard instead.

The capacity of the board is as follows:
    o 35 14-pin DIP's
    o 30 16-pin DIP's
    o 20 24-pin DIP's
    o 15 28-pin DIP's
    o or a combination of the above.

A grid of pads on .100" centers are provided at the top of the board for connectors, headers or discrete components as may be needed.

The edge connector is silk screened on both sides with the 1802 signal names for easy identification.


## PCB

The PCB is double-sided FR4 material and plated in electro-less nickel immersion gold (ENIG). This provides good protection from oxidation on the edge card fingers.

You will find best results working on this board using a variable temperature soldering station and a micro soldering tip. In the need of desoldering, a powered vacuum desoldering gun such as the Hakko FR-301 is highly recommended.

> **NOTE:**
The solder pads on the board have very small profiles and are easily damaged by too high or prolonged heat which will cause them to lift from the board.


## Assembly

1. Wash the board in warm water and mild detergent before assembly to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels and allow to fully dry.
1. Add the filter capacitors.


## Checkout

Power the board and verify that power and ground voltages are correct with respect to the labeled connections.


## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability of fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues. PCB's are warranted against defects in manufacture and will be replaced free of charge. Kit IC's have a 30-day DOA warranty from date of receipt. If you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix

[somaspack@yahoo.com]

<p style="page-break-after: always;">
</p>

# Appendix 

## Appendix I - PCB

<a href="schematics/protoboard_pcb.svg"><img src="schematics/protoboard_pcb.svg" width="448" style="border:1px solid black"/></a>
