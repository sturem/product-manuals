# COSMAC MICROBOARD 5-CARD BACKPLANE INSTRUCTION

__Version 1.0__

Thanks for purchasing my kit for a COSMAC Microboard Backplane PCB! As an RCA1802 fan, I have collected various bits over the years including RCA Microboard format computer boards. I had not yet come across a backplane so I decided to build my own and offer it to the COSMAC community.

The Microboard format is a 4.5" x 7.5" double-sided PCB with 44 edge fingers. The 44-pin bus extends the 1802's 40 pins, plus three voltages and ground. The slow speed of the 1802 and CMOS noise immunity made these backplanes very simple pin-to-pin interconnects.

My version is inspired by the CDP18S675 _'RCA COSMAC Microboard 5-Card Backplane'_ but other than having 5-card slots they differ on dimensions and the external connections. Also the board spacing is .600" versus .400" on the RCA model. 


## Kit Contents

If you purchased a kit, it contains the (1) PCB and (5) 44-pin card edge connectors with guides. These connectors are premium quality and appear to be the same as used on the original RCA backplanes.


## Assembly


* Wash the board in warm water and mild detergent to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels.
* When soldering the edge card connectors, make sure they are parallel with each other and the PCB vias:
  * Insert into board, noting Pin1 on board and socket.
  * Push a socket solidly down and towards Pin1. Tape securely into position.
  * Make sure that socket is sitting flush and level on board surface.
  * Turn over board and solder Pin 1 and Pin Z of the socket.
  * Check alignment of socket and carefully reheat/adjust as needed. When satisfied with alignment, solder remaining pins.
  * Repeat for remaining connectors.


## Mounting
  
| **Important:** the force from card insertion and removal must be taken by the card connectors and **NOT** the backplane PCB. Failure to properly support the connectors may result in the PCB failing. |
| --- |

Mounts can be made using aluminum sheet or angle available at home/hardware stores. Edge card connectors are spaced on .6" centers. To get the correct spacing for drilling the holes, use a .100" perfboard to measure 6 pin holes between each edge card connector.

Use machine screws to securely connect the edge card connector to the PCB. The connectors have 4/40 threaded inserts for machine screws.


## Power

Depending on what your needs are you can supply up to three separate voltages. RCA specified these as: 
* +5V
* +12V/+15V
* -12V/-15V

Many RCA Microboards only require 5V and so this is conveniently supported by the MTA header on each model. An inexpensive USB 5V .5A-1.0A charger is an acceptable power source.


## Checkout

* Apply power to the board and make sure that the correct voltage(s) and polarity are measured on the board.


## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability of fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues.  PCB's are warranted against defects in manufacture and will be replaced free of charge. sIf you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix

[somaspack@yahoo.com]

<p style="page-break-after: always;">
</p>

# Appendix


## Appendix I - Model A Parts List


| Schematic | Description | Supplier/PN
| --- | --- | ---
| CONN1-CONN5 | .156" 44POS FEMALE | Surplus
| PWR1 | CONN HEADER VERT 4POS .100 TIN	| Digikey/ A19431-ND


## Appendix II - Model B Parts List

| Schematic | Description | Supplier/PN
| --- | --- | ---
| CONN1-CONN5 | .156" 44POS FEMALE | Surplus
| EC1 | CARDEDGE 50POS DUAL .100 GREEN | Digikey/151-1386-ND
| CONN6 | CONN HEADER VERT 2POS .100 TIN | Digikey/A19423-ND

<p style="page-break-after: always;">
</p>

## Appendix III - Microboard Pinout


| Pin | Signal | Pin | Signal
| --- |	--- | --- | ---	
| 1 | DMA IN | A | TPA
| 2 | DMA OUT | B | TPB
| 3 | RUN | C | BUS0
| 4 | INTERRUPT | D | BUS1
| 5 | MRD | E | BUS2
| 6 | Q | F | BUS3
| 7 | SC0 | H | BUS4
| 8 | SC1 | J | BUS5
| 9 | CLEAR | K | BUS6
| 10 | WAIT | L | BUS7
| 11 | -5V/-15V | M | A0
| 12 | SPARE | N | A1
| 13 | CLOCK | P | A2
| 14 | NO | R | A3
| 15 | N1 | S | A4
| 16 | N2 | T | A5
| 17 | EF1 | U | A6
| 18 | EF2 | V | A7
| 19 | EF3 | W | MWR
| 20 | +12V/+15V | X | EF4
| 21 | +5V | Y | +5V
| 22 | GND | Z | GND