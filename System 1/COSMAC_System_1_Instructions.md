# COSMAC SYSTEM 1 MICROBOARD ASSEMBLY INSTRUCTIONS - CDP18S701

__Version 1.1__

Thank you for purchasing the COSMAC System 1 Microboard Computer. The System 1 is based on the RCA CDP18S020 COSMAC Evaluation Kit. The idea for this project was to think what RCA may have produced if they were to update the 18S020 evaluation kit in the mid 1980's when the Microboard line of computers was the focus of development. 

Parts of the circuit of the System 1 are a clone of the 18S020. But additions and changes make the System 1 a unique instance of a 1802-based computer as it may have evolved. The System 1 represents what RCA might have produced.

Documentation for the 18S020 COSMAC Evaluation Kit is available on the Internet and contains a wealth of information that is not provided here. See: [Evaluation Kit Manual for the RCA CDP1802 COSMAC Microprocessor](http://www.bitsavers.org/components/rca/cosmac/MPM-203_CDP1802_Evaluation_Kit_Manual_Sep76.pdf)


## Description

The System 1 includes the following:
* CDP1802 clocked at 2Mhz
* Up to 8K of RAM/ROM, organized in 4 x 2K banks
* Individual banks addressable either in low (*0x0000*) or high (*0x8000*) memory
* RS232 serial port with optional inversion of input/output signals
* Run utility (RUNU) and user program (RUNP) push-buttons
* Reset (RESET) push-button
* Step/continue (STEP/CONT) toggle switch to break into single-step mode or resume execution
* Parallel input and output ports with EF or Interrupt handshaking
* Data and 16-bit Address hexadecimal displays 
* User breadboard for expansion
* 44-pin edge fingers with all necessary 1802 signals
* 4.5" x 7.5" RCA Microboard-standard size 

The System 1 requires that a program reside in ROM (EPROM/EEPROM) memory to bootstrap. Typically a monitor program is installed that can communicate via a serial connection to an RS232 terminal or PC, but the ROM could be anything! There are several monitors available including the original RCA UT-4 and Lee Hart's *IdiotMon*. See Appendix III For more details.


## PCB

The PCB is double-sided FR4 material and plated in electro-less nickel immersion gold (ENIG). This provides good protection from oxidation on the edge card fingers.

You will find best results working on this board using a variable temperature soldering station and a micro soldering tip. In the need of desoldering, a powered vacuum desoldering gun such as the Hakko FR-301 is highly recommended.

> **NOTE:**
The solder pads on the board have very small profiles and are easily damaged by too high or prolonged heat which will cause them to lift from the board.


## Circuit Description and Jumper Configuration


### CPU

The 1802 CPU circuit follows standard practice and is the same as found in the CDP18S020 evaluation board including:

* Pull-up resistors on D0 - D7, EF1 - EF4, INT, DMA_IN, DMA_OUT
* LED's for Q, SCO, SC1, CLEAR and WAIT
* Enhanced stability crystal controlled clock oscillator


### Control and Control

The control logic circuit is the same as found on the CDP18S020 evaluation board. Four switches and logic manipulate the 1802 WAIT and CLEAR lines to effect 1802 states - Run, Reset, and Pause: 


#### RUNU (S1)

Places CPU in RUN mode and initates execution at *0x8000* by forcing A15 to high.


#### RUNP (S2)

Places CPU in RUN mode and initates execution at *0x0000*.


#### RESET (S3)

Resets the CPU, after reset the program counter is R0 and starts from *0x0000*.


#### CONT/STEP (S4)

Continuous or single cycle step mode. When in Run mode, if toggle is moved to STEP, the microprocessor is forced into Pause mode as long as STEP remains toggled. The current machine cycle is completed by pressing RUNU or RUNP - depending on whether the program was started from RUNU or RUNP. Subsequent presses of RUNU|RUNP step through the next machine cycles in the program. Moving the toggle to CONT resumes execution of the current program at the next machine cycle.

>**Note:**
When entering Pause mode it is unknown which machine cycle of the current instruction the CPU is in. Thus the break can occur in an fetch or execution cycle and the address displays can be indeterminate. Subsequent presses of RUNU/RUNP will reveal the program address correctly.  


### Memory

The board supports up to 8K of mixed memory, either RAM or ROM, in 4 x 2K banks. For each 2K bank, a jumper (J2, J6, J10, J14) selects whether the bank base address is in low memory (*0x0000*) or the upper 32K of memory (*0x8000*). Two jumpers per bank (J3/J4, J7/J8, J11/J12, J15/J16) selects one of four possible 2K bank offsets (0K, 2K, 4K, 6K) from the base address. A fourth jumper per bank (J1, J5, J9, J13) selects whether the bank supports read/write (RAM) or read-only (ROM).

Most any brand of 2716 EPROM, 28C16 EEPROM or 6116 CMOS Static RAM devices are supported. Any combination of devices is allowed as long as they do not have conflicting addresses. 


### RS232 Serial Port

The RS232 serial port utilizes the MAX232 single chip solution which provides the correct voltage levels required by the standard with only a few discrete capacitors. The circuit capable of serial rates up to 200kbps. The input signal and output signals can be inverted to match the needs of the software driving the port.

J17 selects whether to use EF3 or EF4 for the serial-in signal. J18 and J19 invert the EF or the Q line (respectively) as may be needed to achieve the proper logic state for the software UART branch instructions (B3/BN3 or B4/BN4). 

This allows supporting any of the available software serial port monitors available, which have differing implementations of the EF line handled, and the logic level expected, and which also may differ on the logic state of the serial-out Q line (SEQ/REQ)instructions.


### Parallel Ports

The parallel ports use the standard CDP1852 for I/O. The output port is mapped to N=5, the input port is mapped to N=6. Pullup resistors are provided on the pins of the input port to avoid indeterminate logic levels when read.

J21 selects whether to use INT or EF lines for the input port handshake signal. J20 selects which EF line to use, EF3 or EF4. When using an EF line for input handshaking, do not use the same EF line as the serial port input as this could result in a conflict.


### Address and Data Displays

TIL311 hexadecimal displays are provided for the data and address lines. Displays are connected directly to the data and address bus lines. CLEAR and WAIT are evaluated that when the CPU is in Run mode, the BLANK signal of the TIL311's for both data and address is asserted to active high, blanking the displays.

J22 controls if the data and address displays are blanked (BLANK), or whether they are always on (ON). When in BLANK mode, the displays will be active on RESET and in single-step (STEP). 

>**Note:**
The data displays are not N-line port addressable as in the Elf and other computers. 


### User Breadboard

A 10 x 12 grid of pads is provided for user expansion. This space is large enough to accommodate up to 2 16-pin DIP's, a 20-pin DIP, or even a 24-pin .400" DIP. One row of the area has larger through-holes that will accommodate .100" male headers, this also supplies +5V and ground connections for the breadboard.


## Minimum Configuration

The System 1 board is quite modular and can be configured for individual preference.


### CPU and Memory Only

In this mode the circuit consists of the basic 1802 support and the memory section. The entire 8K of memory space is available if needed. Control is via the Reset (RESET) pushbutton. When pressed the 1802 is put in Reset mode, when released the 1802 automatically enters Run mode. Execution begins at 0x0000 or 0x8000. In this mode there is a trade-off: memory slots are mapped to either 0x0000 or 0x8000 but cannot be intermixed.

* Populate only: U1 - U10 and 0 to 4 memory banks U11 - U14, C1 - C2, C3, C19 - C20, R1 - R3, RN1 - RN2, S3, LED 1, LED 4
* Jumper LNK1:1-2 and LNK2:1-2
* For LO addressing, jumper U10:6 to GND
* For HI addressing, jumper U10:6 to +5V
* Set memory bank LO/HI jumpers to the same level as the memory mapping 

>**Note:**
This configuration severely limits the use of the board in a larger system. Memory selection is limited to __only__ memory in low or high memory space without __any__ bus arbitration.


### Parallel Ports

* Add U21 - U23, J20, J21, RN3, CONN3, CONN4


### Hex Displays


* Add U24 - U29, J22


### RS232 Serial Port

* Add U20, C4-C8, J17-J19, CONN1


## Assembly

1. Wash the board in warm water and mild detergent before assembly to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels and allow to fully dry.
1. Add the crystal, resistor networks, discrete resistors, diodes and capacitors.
1. Add IC sockets (optional).
1. Add the discrete LED's for Q, SC0, SC1, WAIT and CLEAR. The long lead (??) of the LED goes into the square pad.
1. Add the .100" male headers for jumpers and the 12-pin female headers or the parallel ports.
1. Add the switches
1. Add the DB9 connector, securely solder the mounting lugs to the board
1. Clean the board of flux residue using 90% isopropyl alcohol or suitable commercial product. Dry thoroughly.
1. Add jumpers to header locations for memory, RS232 and input ports.
1. If soldering IC's, complete the Checkout Procedure steps 1-4 before proceeding.


## Hardware Checkout Procedure

Note: you will need a digital logic probe and multi-meter to perform circuit checkout.
1. Top side: double-check correct placement and orientation of ALL components.
1. Bottom side: check that all pins have been soldered and that all joints are bright and shiny.
1. Check voltage polarity is correct: connect the power source and verify using a multi-meter that IC1-40 shows 5V and IC1-20 shows 0V.
1. Insert all socketed IC's observing polarity.
1. Power-on the board.
1. Using a logic probe, check that U1-39 shows a pulsing signal.
1. Press the Reset button, Address displays should should show '0000 0000'.
1. Press the RUNU button, Address displays should dim and show all elements cycling.
1. Press the Reset button, Address displays should should show '0000 0000'.
1. Press the RUNP button, Address displays should dim and show all elements cycling.


## Software Checkout

For basic functional checkout of the control and memory circuits, I recommend burning an EPROM/EEPROM with a simple 'Q Blink' program. The code can be located at either *0x0000* or *0x8000* and executed by the RUNP or RUNU programs, respectively. Once assured that the hardware is functional, proceed to configure for a monitor ROM.


## Single-Step Checkout

To check that the single-step circuit is functioning as expected:
1. Press either RUNU or RUNP to execute a ROM program such a Q blink.
1. Set the Single Step toggle to break (STEP)
1. If the displays were blanked (J22), the address digits should now show where execution has been halted and the data digits should show a value (may be actual data or an indeterminate internal bus state of the 1802).
1. Press RUNU or RUNP (same as above) to advance the program counter (PC), noting the address and data display values. Make sure the program steps properly based on the program instructions and logic and displays the correct address and data values.
1. Set the single step toggle to CONT, program execution should continue.


## RS232 Configuration and PC Integration

The System 1 RS232 serial port is intended to be connected to a PC using a 9-pin null-modem cable. For newer PC's without an RS232 serial port, USB to RS232 converters work fine. Serial converters based on the PL2303 chipset are well supported by the usual PC OS's. The [TrendNet TU-S9](https://www.amazon.com/gp/product/B0007T27H8) paired with a [DB9 RS232 Serial Null Modem Cable, Female to Female](https://www.amazon.com/gp/product/B00QM8ZP5E) works well for this application. 

Any terminal program that can connect to a serial port provided by the OS should work just fine. On Linux I recommend *minicom*.  Suggestions for other OS's and instructions on how to configure a serial port and terminal for a particular OS is beyond the scope of this manual. Please consult your favorite search engine for more details.

When setting up the terminal connection, the serial connection should be:
* 7 bits
* Mark parity
* 1200 baud

>**Note:**
Other bit/parity configurations may also work.
The max baud of the bit-bang serial port is 1200 baud (1.2kbps).


### Serial Loopback Test

The simple serial loopback program presented here can be used to test the serial connection. When run, it will echo input from the remote terminal back to the remote terminal. Format is A18 ASM.

```
INIT
    TITL            "SerialLoopback - reads input on EF3 and replays on Q"
    ORG	0H
START    
    REQ             ; reset Q
LO
    BN3 START       ; if EF3 low, Q low
    SEQ             ; otherwise set Q
    BR LO           ; and branch to test
    END
```

## Monitor Configuration and Checkout

Program and insert the serial monitor EPROM/EEPROM into any open bank. Configure the jumpers on this bank as follows: A0, B0, HI, ROM. The UT series of serial monitors expect RAM available for a stack in the lower 1K of memory, make sure you have a RAM IC in another bank configured as: A0, B0, LO, RAM.

The bit-bang monitors use B3/BN3 or B4/BN4 instructions. Both the signal (EF3 or EF4) and the level (branch if (B), branch if not (BN)) are accomodated in hardware (J17, J18). The RS232 port must be configured to a complementary state with the monitor. This takes a bit of experimentation to get the jumpers set correct so that the System 1 is communicating successfully with a remote RS232 terminal.

Here are some tips on debugging and analyzing the results of testing:

* If nothing happens when trying to handshake with the System 1, then likely the EF selection is wrong.  
* If the Q LED flashes but nothing comes back to the remote terminal, System 1 is sending data but it's not getting to the remote terminal. Make sure you are using a null-modem cable.
* Garbled characters coming back to the terminal is actually good! It means that there was a handshake between the remote terminal and the System 1 board. Try reversing the Q line jumper first and then the EF logic level jumper.


## Expansion

The COSMAC System 1 board is designed to be the core of an expanded system using the Microboard standard. All CPU signals are extended to the System 1 edge fingers. No signals other than CLOCK are buffered by the System 1 board, similar to other RCA CPU boards. Due to limited fan-out of the 1802, all other Microboards in a system should buffer any lines connected to the Microboard bus. 

The System 1 board will work with other boards in a rack that properly handle bus contention, but there are several considerations

* For memory boards there must not be any overlapping memory banks that would lead to non-unique address selection across multiple boards in a configured system. In such an instance it is necessary to disable (move to non-used address) the affected banks of one (or more) of the boards.
* In **most** cases when using a memory board such as the 64K RAM/ROM, the System 1's memory should be disabled/unused as this does not have bus arbitration circuitry.

Other products are available to complement the System 1 including:

* Microboard backplanes
* 64K RAM/ROM memory
* Prototyping boards - plain and with Microboard buffering

Please checkout the [products](PRODUCTS.md) catalog for more details.

## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability of fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues.  PCB's are warranted against defects in manufacture and will be replaced free of charge. Kit IC's have a 30-day DOA warranty from date of receipt. If you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix

[somaspack@yahoo.com]

---

# Appendix


## Appendix I - Parts List

| Component | Description |	Source
| --------- | ----------- | ------
| C1, C2 |22pf disc cap. | DigiKey 490-8653-ND 
| C3 | 1mf disc cap (optional) | Jameco 81509
| C4 - C8 | 1mf disc cap. | Jameco 81509
| C9 - C18 | .1mf disc cap. (optional) | Jameco 25523
| C19, C20 | 22mf 25V radial electrolytic cap. | Jameco 93739
| CONN1 | DE9M PCB connector | Jameco 104943
| CONN2 | MTA3 Male (optional) | DigiKey A19470-ND
| CONN3, CONN4 | 12 pin .100" female header | Jameco 200783 
| J1 - J4, J5 - J8, J9 - J12, J13 - J16 | dual row 12-pin male header | Jameco 203810
| J17 - J22 | 3-pin .100" male header | Jameco 109576
| LED1 - LED5 | 3mm Red LED | Jameco 333973
| R1 | 10Mohm 1/4W resistor | Jameco 691817
| R2, R3 | 22Kohm 1/4w resistor | Jameco 691180
| RN1 | 22Kohm x 7 resistor network | DigiKey 4608X-1-223LF-ND 
| RN2, RN3 | 22Kohm x 8 resistor network | Jameco 1971407
| S1, S2 | SPDT NO Pushbutton Switch | DigiKey 360-3265-ND
| S3 | SPDT NO Pushbutton Switch | DigiKey 360-2618-ND
| S4 | SPDT NO Toggle Switch | Digikey 360-1751-ND
| U1 | CDP1802 | surplus/eBay
| U2, U15 | CD4049 | Jameco 13055
| U3, U4 | CD4042 | DigiKey 296-2049-5-ND 
| U5 - U7 | CD4070 | Jameco 13258
| U8, U9 | CD4072 | Jameco 676078
| U10 | CD4071 | DigiKey  296-2062-ND 
| U11 - U14 | 6116/2716 | surplus/eBay
| U16, U17, U21 | CD4011 | Jameco 893283
| U18 | CD4001 | Jameco 12562
| U19 | CD4013 | Jameco 12677
| U20 | MAX232 | Jameco 698576
| U21 | CD4011 | Jameco 893283
| U22, U23 | CD1852 | surplus/eBay
| U24 - U29 | TIL 311 | surplus/eBay
| X1 | 2Mhz HC48 Crystal | DigiKey CTX068-ND
| - | Jumper Shorting Block | Jameco 112432

Miscellaneous items (optional or user supplied):
* ROM monitor in EPROM/EEPROM
* IC sockets
* Backplane for expansion
* Power connection


### Kit Contents 

If you purchased a kit, it contains all the non-optional parts listed above. To keep costs low and for personal preferences, IC sockets are provided for the CPU (U1) and the memory bank (U11 - U14).

>**Important:**
Please note that a ROM monitor is not supplied with the kit due to licensing and/or copyrights. The monitor ROM is the personal preference and the responsibility of the end user. Any of the serial bit-bang monitors should work fine. IDIOTMON, RCA's UT4, and variants have been tested. See Appendix IV. 

<p style="page-break-after: always;"></p>


## Appendix II - Schematics


### CPU

<p style="page-break-after: always;">
    <a href="schematics/cosmac_cpu_1b.svg"><img src="schematics/cosmac_cpu_1b.svg" width="550" style="border:1px solid black"/></a>
</p>


### CPU Control Logic

<p style="page-break-after: always;">
    <a href="schematics/cosmac_control.svg"><img src="schematics/cosmac_control.svg" width="700" style="border:1px solid black"/></a>
</p>


### Data and Address Display

<p style="page-break-after: always;">
    <a href="schematics/cosmac_data_and_address_display.svg"><img src="schematics/cosmac_data_and_address_display.svg" width="700" style="border:1px solid black"/></a>
</p>


### Memory & Address Decoding Logic

<p style="page-break-after: always;">
    <a href="schematics/cosmac_8k_ram_rom_2k_select_logic_2.svg"><img src="schematics/cosmac_8k_ram_rom_2k_select_logic_2.svg" width="700" style="border:1px solid black"/></a>
</p>


### RS232 Serial Port

<p style="page-break-after: always;">
    <a href="schematics/cosmac_rs232.svg"><img src="schematics/cosmac_rs232.svg" width="700" style="border:1px solid black"/></a>
</p>


### Parallel Output Port

<p style="page-break-after: always;">
    <a href="schematics/cosmac_parallel_output_port.svg"><img src="schematics/cosmac_parallel_output_port.svg" width="700" style="border:1px solid black"/></a>
</p>


### Parallel Input Port

<p style="page-break-after: always;">
    <a href="schematics/cosmac_parallel_input_port.svg"><img src="schematics/cosmac_parallel_input_port.svg" width="700" style="border:1px solid black"/></a>
</p>


### COSMAC Microboard Bus

<p style="page-break-after: always;">
    <a href="schematics/cosmac_expansion_44edge.svg"><img src="schematics/cosmac_expansion_44edge.svg" width="700" style="border:1px solid black"/></a>
</p>


## Appendix III - PCB

<a href="schematics/system-1-board-net.svg"><img src="schematics/system-1-board-net.svg" width="535" style="border:1px solid black"/></a>


## Appendix IV - Serial Monitors

Herb Johnson has done a nice job collecting old 1802 information and software including several serial monitor programs. Herb also discusses at length the issues with software serial interfaces and the 1802. This information was essential in bootstrapping my development. Thanks for the help Herb!

The links provided are provided for convenience:
* ["IDIOT" monitor, serial interface, for 1802 Membership Card](http://www.retrotechnology.com/memship/idiot_ramrom.html)
* [COSMAC 1802 "IDIOT" monitor Software](http://www.retrotechnology.com/memship/mship_idiot.html)
* [RCA UT4 ROM](http://www.retrotechnology.com/memship/UT4_rom.html)
* [Serial interface for 1802 Membership Card](http://www.retrotechnology.com/memship/mem_rom_serial.html)
* [Testing the serial interface (Rev G & later, customer mods for earlier)](http://www.retrotechnology.com/memship/mship_test.html)

There are a few things to consider when using these monitors:
* All implement a stack in RAM, make sure the stack is within your physical RAM address
* All use either EF3 or EF4 for serial input, but the level of EF may either be interpreted as low or high. You can either flip the branching opcodes (BZ/BNZ) for checking EF line status in the sources or set the jumpers on the System 1, or both if you wish.


## Appendix V - Notes on Programming EPROM's/EEPROM's

When burning a program to EPROM/EEPROM which uses long branch instructions, it's important to set the origin of the code to the proper starting address. If the code burned on a device is to be run at *0x8000*, then the source code 'origin' should be the same. If this is not followed, when long branch instructions are encountered they will vector to low memory and this will be unexpected and incorrect. Short branch instructions are not affected by this.

In some programming software, when importing a HEX file, the use of an origin offset will result in the programmer thinking that the code should be at some other address than *0x0000* in the EPROM/EEPROM! This is of course wrong. Use of a negative offset when importing the HEX file into the programming software resolves the issue.
