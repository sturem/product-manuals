# Spacktronix Product List


## Prices and Shipping

* All prices are in US dollars and **do not** include shipping
* Payment can be made via PayPal or Venmo. Please no cash, checks or cards
* For shipping please contact me and I will quote based on your choice:
    * US orders are via USPS First Class or Priority Mail
    * Canandian orders are via US Postal Service First Class International Mail unless otherwise requested
    * International orders are welcome, please inquire about shipping to your residence


## Configurations

I offer several product configuration options:

* PCB - bare board, printed instructions and parts list. __All PCB's with edge fingers are ENIG (gold) plated__
* Limited Edition Kit - PCB (above) + all discrete components (see descriptions for details)

Most parts for boards are commonly available from US suppliers such as Jameco or Digikey. Some components are vintage, meaning hard-to-find or more expensive and may require using surplus suppliers or even dreaded eBay. For parts supplied in kits, only new and NOS vintage parts from reputable sources are offered. I do not use remarked/remanufactured IC's imported from China.


## Documentation

* Enhanced [documentation and additional](https://gitlab.com/cosmac-public/product-manuals/tree/master) resources are online and updated on a periodic basis. 


---

# Microboards


## What is a Microboard?

From the late 1970's through the 1980's RCA Solid State Division offered computer systems based on their 1800-series microprocessor in a 4.5" x 7.5" PCB format called a _'Microboard'_. Boards were interconnected via a 44-pin interface bus carrying the 1802 signals and power. The product line included single board computers, memory and I/O boards that could be plugged into a variety of backplanes and racks systems allowing for customized applications. The ultimate end of this product line was the _'RCA Microboard Computer Development System (MSDS)'_ which featured a 25-card backplane with a complete microcomputer system and software development tools and programming languages.

The retro-creations listed here are inspired by historical RCA Microboard designs (noted in the descriptions) with changes and additions as may be needed for my purposes.


## System 1 Single Board Computer (SBC) - CDP18S701

The System 1 SBC is based on the CDP18S020 _'RCA COSMAC Evaluation Kit'_, circa 1977. The 18S020 demonstrated many of the features that were standard in the RCA COSMAC architecture as described in _'MPM-201B User Manual for the CDP1802 Microprocessor'_:
* RAM/ROM interfacing
* CPU control
* Data, address and CPU status display
* I/O selection using N lines
* Parallel I/O with EF or Interrupt handshaking
* Serial I/O - 20ma TTY and RS232C (+/- 12V)

The 18S020 featured:
* CDP1802 clocked at 2Mhz
* 256-bytes of RAM, expandable to 4K of RAM
* 32-bytes of stack RAM
* 512-bytes of ROM containing the _Utility Program_ UT4, which provided rudimentary memory and program manipulation via the serial port and an external terminal
* Run utility (RUNU) and user program (RUNP) push-buttons
* Reset (RESET) push-button
* Step/continue (STEP/CONT) toggle switch to break into single-step mode or resume execution
* User breadboard for expansion

The System 1 implements and improves upon features of the 18S020 including:
* Up to 8K of RAM or ROM, organized in 4 x 2K banks
* Individual banks addressable either in low (0x0000) or high (0x8000) memory
* +5V RS232 serial port with optional inversion of input/output signals
* Hexadecimal displays for data and address, displays can be blanked when in RUN mode
* 44-pin edge fingers with all 1802 signals
* 4.5" x 7.5" RCA Microboard standard size

The System 1 SBC requires that a program reside in ROM (EPROM/EEPROM) memory to bootstrap. The original RCA [UT4](https://gitlab.com/cosmac-public/ut4) is perfectly suited for the task. The instructions discuss monitors in more detail.

* [Product Manual](https://gitlab.com/cosmac-public/product-manuals/blob/master/System%201/COSMAC_System_1_Instructions.md)

>*Note:*
ROM monitor is not included in kits.  

| <a href="product_images/system1_built.png"><img src="product_images/system1_built.png" width="320" style="border:1px solid black" title="System 1 Prototype" alt="System 1 Prototype"></a> | <a href="product_images/system1_pcb.png"><img src="product_images/system1_pcb.png" width="320" style="border:1px solid black" title="System 1 PCB" alt="System 1 PCB"></a> | <a href="product_images/system1_kit.png"><img src="product_images/system1_kit.png" width="320" style="border:1px solid black" title="System 1 Kit" alt="System 1 Kit"></a>

__Status: Available__

* PCB: $14
* Limited Edition Kit (PCB + switch set, all discrete components including 4K RAM, machine pin sockets for CPU, memory and I/O devices): $120
* Additional 2K RAM (6116 type): $2 each


## System 1 64K RAM/ROM Microboard - CDP18S638

This board was inspired by the CDP18S626 _'RCA COSMAC Microboard 32/64-Kilobyte EPROM/ROM/RAM'_ and CDP18S629 _'RCA COSMAC Microboard 32-Kilobyte RAM Microboard_'.

The CDP18S638 provides up to 64K of mixed RAM and ROM in an 1802-based Microboard architecture system. Two banks are provided:

* 32k CMOS RAM using a 62256 family device with a base address located at either 0x0000 or 0x8000.
* 32k RAM/ROM in 4 x 8K segments configurable within the 64k address block, with a base address at 0x0000 or 0x8000. Uses 2732 EPROM, 28C32 EEPROM or 6264 Static RAM family devices and can be mixed as desired within the address space

The board is compatible with the System 1 SBC and provides circuitry for address/data bus contention with other boards in a system.

* [Product Manual](https://gitlab.com/cosmac-public/product-manuals/blob/master/64K%20RAM-ROM/COSMAC_System_1_64K_RAM-ROM_Instructions.md)

| <a href="product_images/64k_ramrom_built.png"><img src="product_images/64k_ramrom_built.png" width="320" style="border:1px solid black" title="System 1 64k RAM/ROM Built" alt="System 1 64k RAM/ROM Built"> </a> | <a href="product_images/64k_ramrom_kit.png"><img src="product_images/64k_ramrom_kit.png" width="320" style="border:1px solid black" title="System 1 64k RAM/ROM Kit" alt="System 1 64k RAM/ROM Kit"></a> | <a href="product_images/64k_ramrom_pcb.png"><img src="product_images/64k_ramrom_pcb.png" width="320" style="border:1px solid black" title="System 1 64k RAM/ROM PCB" alt="System 1 64k RAM/ROM PCB"></a>

___Status: Available___

* PCB: $12
* Limited Edition Kit (PCB + all discrete components including (1) 32k RAM, machine pin sockets for memory devices): $40


## Unbuffered Proto Microboard - CDP18S702

This Microboard is inspired by the CPD18S659 _'RCA COSMAC Microboard Breadboard'_ and features:

* Standard Microboard-sized prototyping PCB allows for convenient experimenting or building one-off RCA 1802 Microboards
* 1802 signal names are silk screened on both sides for easy identification
* Accepts a variety of connectors for off-board interfacing
* Microboard bus signals are provided unbuffered
* The capacity of the board is as follows:
    * 35 14-pin DIP's
    * 30 16-pin DIP's
    * 20 24-pin DIP's
    * 15 28-pin DIP's
    * or a combination of the above

* [Product Manual](https://gitlab.com/cosmac-public/product-manuals/blob/master/Protoboard/COSMAC_System_1_Protoboard_Instructions.md)

<a href="product_images/protoboard_pcb.png"><img src="product_images/protoboard_pcb.png" width="320" style="border:1px solid black" title="System 1 Protoboard PCB" alt="System 1 Protoboard PCB"></a>

__Status: Available__

* PCB: $12


## Buffered Proto Microboard - CDP18S705

This Microboard was created in from my need to simplify prototyping of circuits that use data and address lines such as memory, I/O and display boards. 

* Standard Microboard-sized prototyping PCB allows for convenient experimenting or building one-off RCA 1802 Microboards
* Tri-state data bus buffering to prevent bus contention
* Address bus buffering and latching for upper 8 bits
* TPA, MRD, MWR and RUN buffered inputs
* Additional buffering for 4 user-selectable signal lines is provided
* All buffered signals are arranged on .100" centers for either a header or solder connection 
* A grid of pads on .100" centers are provided at the top of the board for connectors, headers or other components as may be desired
* The capacity of the board is as follows:
    * 25 14-pin DIP's
    * 20 16-pin DIP's
    * 15 24-pin or 28-pin DIP's
    * or a combination of the above

<a href="product_images/buffered_protoboard_prototype.png"><img src="product_images/buffered_protoboard_prototype.png" width="320" style="border:1px solid black" title="System 1 Buffered Protoboard Prototype" alt="System 1 Buffered Protoboard Prototype"></a>

__Status: Available__

* PCB: $12


## COSMAC Elf Microboard

Step back in time when programming a computer meant setting individual toggle 'bit' switches in binary!

40th Anniversary COSMAC Elf Computer presented as an RCA 'Microboard' format PCB! This version of the Elf follows the design presented in the Popular Electronics series from 1976. The RAM was expanded to 1K but the circuit is functionally the same otherwise.

The Elf can be the basis of an expanded system by utilizing a 5-Card Microboard Backplane and a Microboard Prototyping board. The COSMAC Elf was used in this manner to the jumpstart development of the memory and RS-232 circuits used in the System 1.

* [Product Manual](https://gitlab.com/cosmac-public/product-manuals/blob/master/Elf/COSMAC_Elf_Instructions.md)

| <a href="product_images/cosmac_elf_built.png"><img src="product_images/cosmac_elf_built.png" width="320" style="border:1px solid black" title="COSMAC Elf Microboard Built" alt="COSMAC Elf Microboard Built"></a> | <a href="product_images/cosmac_elf_pcb.png"><img src="product_images/cosmac_elf_pcb.png" width="320" style="border:1px solid black" title="COSMAC Elf Microboard PCB" alt="COSMAC Elf Microboard PCB"></a>

__Status: Available__

* PCB: $12


---

# Microboard Backplanes

These backplanes are inspired by the CDP18S675 _'RCA COSMAC Microboard 5-Card Chassis'_ being electrically equivalent but differing in dimensions, expansion and power connections. 

5-card backplanes hit the sweet spot in terms of size. Later RCA Microboards showed an increase in integration and function compared with earlier models. This allowed for using smaller backplanes and two core Microboards:

* CPU
* Memory expansion - RAM/ROM

plus three others:
* Serial I/O
* Analog I/O
* Parallel I/O
* Video and keyboard
* Storage

to assemble complete microsystems.

* [Product Manual](https://gitlab.com/cosmac-public/product-manuals/blob/master/Backplanes/COSMAC_Microboard_Backplane_Instructions.md)

## Backplane Type A

5-card, 44-pin, .256" pitch backplane for RCA Microboards. 4-pin MTA power connector for ground, +5V, -5V, +12V. 

<a href="product_images/backplane_a_pcb.png"><img src="product_images/backplane_a_pcb.png" width="320" style="border:1px solid black" title="Backplane Type A PCB" alt="Backplance Type A PCB"></a>

__Status: Available__

* PCB: $6 + shipping
* Limited Edition Kit: $72


## Backplane Type B

5-card, 44-pin, .256" pitch backplane for RCA Microboards. ENIG (gold) plated, 50 - pin edge connector for debugging or daisy chaining multiple backplanes. 2-pin MTA power connector for ground and +5V.

<a href="product_images/backplane_b_pcb.png"><img src="product_images/backplane_b_pcb.png" width="320" style="border:1px solid black" title="Backplane Type B PCB" alt="Backplance Type B PCB"></a>

__Status: Available__

* PCB: $8 + shipping
* Limited Edition Kit: $74


## Female Edge Connectors with Card Guides

Offered are the same edge card guides that RCA used in their small backplanes. These quality edge connectors provide mechanical support for a continuous electrical signal, endure repeated card cycling and general (ab)use. As can be seen in the pictures, these have been mounted on aluminum rails (material found at local home hardware store) and secured from below with machine screws. This has proven itself to be very robust with zero issues during my own use.

* Contact Material: Copper, Nickel, Tin Alloy 	
* Contact Finish: Gold 

| <a href="product_images/cardedge_1.png"><img src="product_images/cardedge_1.png" width="320" style="border:1px solid black" title="Edge card guides" alt="Edge card guides"></a> | <a href="product_images/cardedge_2.png"><img src="product_images/cardedge_2.png" width="320" style="border:1px solid black" title="Edge card guides" alt="Edge card guides"></a> | <a href="product_images/cardedge_3.png"><img src="product_images/cardedge_3.png" width="320" style="border:1px solid black" title="Edge card guides" alt="Edge card guides"></a>

__Status: Available__

* Edge card with guides: $14 (each)


---

# IC's

Select RCA NOS and recycled parts from US suppliers. __I do not offer remarked/remanufactured IC's imported from China!__

### CDP1802ACE

Harris, 3.2Mhz @ 5V, PDIP

__Condition:__ _NOS_

__Status: Available__, Price: $4 (each)

### CDM6116AE2

200ns 2K x 8 Static CMOS RAM

__Condition:__ _NOS, RCA tubes_

__Status: Available__, Price: $2 (each)


### CDM62256

64k x 8 CMOS Static RAM

__Condition:__ _Very clean pulls_

__Status: Available__, Price: $4 (each)


### CDP1852CE

Byte-wide parallel input/output port

__Condition:__ _NOS, RCA tubes_

__Status: Available__, Price: $3 (each)
